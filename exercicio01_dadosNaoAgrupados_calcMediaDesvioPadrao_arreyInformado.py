#!/usr/bin/env python
# coding: utf-8
#1a Calculando média e desvio padrão de dados NÃO agrupados com numpy - informando a série em um array
valores= [2, 4, 3, 5]
import numpy as np
print('media com numpy', np.average(valores))
print('variancia amostral com numpy', np.var(valores))
print('desvio padrao com numpy', np.std(valores))
from math import ceil
print('desvio padrao populacional com numpy', ceil(np.std(valores)* 10000.0) / 10000.0)
