#!/usr/bin/env python
# coding: utf-8
#1. Calculando média e desvio padrão de dados NÃO agrupados (usando numpy) - interativo
print('Informe o número de elementos do array:')

s = []
val = (int(input()))
for c in range(0,val):
    print('Informe um elemento da série:')
    s.append ( int(input()) )

import numpy as np
print('media com numpy', np.average(s))
print('variancia populacional com numpy', np.var(s))
print('desvio padrao com numpy', np.std(s))
from math import ceil
print('desvio padrao populacional com numpy', ceil(np.std(s)* 10000.0) / 10000.0)
