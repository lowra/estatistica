#!/usr/bin/env python
# coding: utf-8
#1b Calculando média e desvio padrão e variância de dados NÃO agrupados usando minhas funções
import operacoes

a = [4,5,7,8,10,11,12,15,19,20]
media = operacoes.media(a)
difmedia = operacoes.difmedia(a,media)
potenciadif = operacoes.potenciadif(difmedia)
variancia = operacoes.variancia(potenciadif)
