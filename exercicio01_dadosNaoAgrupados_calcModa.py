#!/usr/bin/env python
# coding: utf-8
#Calculando a Moda  (M)
#Basta colocar os valores no ROL e depois verificar qual valor ocorreu com maior frequência.
import numpy as np
import statistics
from collections import Counter
dados = [24,23,22,28,21,23,24,21,25,25,26,21,25,26,25]
print('Dados:',dados)
rol = np.sort(dados) # rol dos dados
print('Rol:',rol)
print('Ocorrências:',Counter(dados)) #contar ocorrências de valores em uma lista
moda = statistics.mode(dados)
print('Moda:',moda)
