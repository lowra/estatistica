#!/usr/bin/env python
# coding: utf-8#2a Média e desvio padrão de dados AGRUPADOS por freqüência simples (sem usar numpy) - informando a série em
#um array
#series original = [4,2,4,6,4,8,2,4,4,2,6,6,6,6,6,6,8,8,8,8,8,8,8,8]
series = [2, 4, 6, 8]
freq =  [3, 5, 7, 9]
#xf = [6, 20, 42, 72]
#por falta de tempo, vou usar numpay apenas para multiplicar os arrays:
import numpy as np
xf=np.array(series)*np.array(freq)

media = round((sum (xf)/sum(freq)),2)
print('média', media)
# difmedia calcula a diferença entre o valor de cada ocorrência e o valor médio das ocorrências ("media").
difmedi = []
for i in series:
    difmedia = round((i - media),2)
    print('Diferença valor - média:', difmedia)
    difmedi.append (difmedia)
# potenciadif recebe o valor das diferenças calculadas e eleva ao quadrado.
potenciadi = []
for i in difmedi:
    potenciadif = (round((i**2),2))
    print('Potência da diferença de valor - média:', potenciadif)
    potenciadi.append (potenciadif)
# variancia recebe a soma dos valores das diferenças elevadas ao quadrado e divide pela quantidade de ocorrências (var. populacional)
# ou divide pela quantidade de ocorrências menos 1 (var. amostral).
pop = round((sum(potenciadi)/len(potenciadi)),2)
print('Variância populacional:', pop)
amo = round((sum(potenciadi)/((len(potenciadi))-1)),2)
print('Variância amostral:', amo)
variancia = [pop, amo]
# desvio recebe as variâncias e calcula a raiz quadrada retornando os desvios calculados.
import math
e4 = math.sqrt(pop)
e5 = math.sqrt(amo)
print('Desvio padrão populacional:', round(e4,4))
print('Desvio padrão amostral:', round(e5,4))
