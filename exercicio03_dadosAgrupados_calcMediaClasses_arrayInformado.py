#!/usr/bin/env python
# coding: utf-8
"""
Classes 1-3, 3-5, 5-7, 7-9, 9-11 --> classes [(1,3),(3,5),(5,7),(7,9),(9,11)]
Freq [2, 4, 8, 4, 2 ]
"""
import numpy as np
classes = [[1,3],[3,5],[5,7],[7,9],[9,11]]
freq = [2, 4, 8, 4, 2 ]
print('Classes:',classes)
print('Freqüências:',freq)
x = []
xf = []
for i in range(0,len(classes)):
    #print (classes[i])
    #print (classes[i][0])
    #print (classes[i][1])
    #print ((classes[i][1])+(classes[i][0]))
    #print (((classes[i][1])+(classes[i][0]))/(2))
    ## x.append ((((classes[i][1])+(classes[i][0]))/(2)))
    x.append (int((((classes[i][1])+(classes[i][0]))/(2))))
    #print (x[i]) 
    #print (freq[i])
    xf.append ( (x[i]) * (freq[i]))

X = sum(xf)/sum(freq)   
print('x:',x)
print('xf:',xf)
print ('Média aritimética "X":',X)
