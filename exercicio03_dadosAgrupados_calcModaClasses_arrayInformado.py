#!/usr/bin/env python
# coding: utf-8
"""
Mo = l + (d1 / d1+d2 ) * h
onde:
d1 = diferença entre a frequência da classe modal e a anterior;
d2 = diferença entre a frequência da classe modal e a posterior;
l = limite inferior da classe modal;
hi = amplitude da classe modal (Li – li).
"""
