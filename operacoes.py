# Funções para cálculo de desvio padrão amostral e populacional
# media recebe uma série de ocorrências (vetor) a ser analisada, soma todos os valores e divide pelo número de elementos da série.
#dados não agrupados!!
def media(vetor): 
	media = sum (vetor)/len(vetor)
	print('média', media)
	return media
# difmedia calcula a diferença entre o valor de cada ocorrência e o valor médio das ocorrências ("media").
def difmedia(vetor, media):
	difmedi = []
	for i in vetor:
		difmedia = round((i - media),2)
		print('Diferença valor - média:', difmedia)
		difmedi.append (difmedia)
	return difmedi
# potenciadif recebe o valor das diferenças calculadas e eleva ao quadrado.
def potenciadif(vetor):
	potenciadi = []
	for i in vetor:
		potenciadif = (round((i**2),2))
		print('Potência da diferença de valor - média:', potenciadif)
		potenciadi.append (potenciadif)
	return potenciadi
# variancia recebe a soma dos valores das diferenças elevadas ao quadrado e divide pela quantidade de ocorrências (var. populacional)
# ou divide pela quantidade de ocorrências menos 1 (var. amostral).
def variancia(vetor):
	pop = round((sum(vetor)/len(vetor)),2)
	print('Variância populacional:', pop)
	amo = round((sum(vetor)/((len(vetor))-1)),2)
	print('Variância amostral:', amo)
	variancia = [pop, amo]
	return variancia 
# desvio recebe as variâncias e calcula a raiz quadrada retornando os desvios calculados.
def desvio(vetor):
	import math
	despop = round(math.sqrt(vetor[0]),4)
	print('Desvio padrão populacional:', despop)
	desamo = round(math.sqrt(vetor[1]),4)
	print('Desvio padrão amostral:', desamo)
	desvios = [despop, desamo]
	return desvios